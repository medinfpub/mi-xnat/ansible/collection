# nfs_server

## Parameters

### `nfs_server_state`

- default:

```
"enabled"

```

### `nfs_server_bind_mounts`

- default: `""`

### `nfs_server_exports`

- default: `""`

