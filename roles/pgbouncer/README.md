# pgbouncer

## Parameters

### `pgbouncer_state`

- default:

```
"enabled"

```

### `pgbouncer_ini`

- default:

```
|
  [databases]
  xnat = host=postgreSQL1.miskatonic.edu port=5432 dbname=xnat
  [pgbouncer]
  logfile = /var/log/postgresql/pgbouncer.log
  pidfile = /var/run/postgresql/pgbouncer.pid
  listen_addr = localhost
  listen_port = 6432
  unix_socket_dir = /var/run/postgresql
  auth_type = md5
  auth_file = /etc/pgbouncer/userlist.txt
  admin_users = xnat

```

### `pgbouncer_userlist`

- default:

```
"\"xnat\" \"\""

```

