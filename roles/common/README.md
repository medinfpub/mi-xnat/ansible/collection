# common

## Parameters

### `ansible_controller_user`

- default:

```
"cloud"

```

### `fqdn`

- default:

```
"{{ inventory_hostname }}"

```

### `ip`

- default: `""`

