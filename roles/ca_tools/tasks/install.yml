---
- name: Clone or update ca-tools source code repository
  ansible.builtin.git:
    repo: "{{ ca_tools_repo_url }}"
    version: "{{ ca_tools_version }}"
    dest: "{{ ca_tools_repo_path }}"
    clone: true
    update: true
  become: true

- name: Install ca-tools
  community.general.make:
    chdir: "{{ ca_tools_repo_path }}"
    target: install
  become: true

- name: Create all group inventory group_vars directory
  ansible.builtin.file:
    name: "{{ ca_tools_ca_cert_dir }}"
    mode: 0755
    state: directory

- name: Create all host_vars server cert directories
  ansible.builtin.file:
    name: "{{ host_vars_dir }}/{{ item.name }}/.ssl/server"
    mode: 0755
    state: directory
  loop: "{{ hostvars | dict2items(key_name='name', value_name='vars') }}"

- name: Create all host_vars client cert directories
  ansible.builtin.file:
    name: "{{ host_vars_dir }}/{{ item.name }}/.ssl/client"
    mode: 0755
    state: directory
  loop: "{{ hostvars | dict2items(key_name='name', value_name='vars') }}"

- name: Generate CA
  ansible.builtin.command: generate-ca
  args:
    creates: "{{ ca_tools_ca_cert_dir }}/ca.pem"
  environment:
    CA_CERT: "{{ ca_tools_ca_cert_dir }}/ca.pem"
    CA_KEY: "{{ ca_tools_ca_key_dir }}/ca-key.pem"
    CA_SUBJECT: "{{ ca_tools_ca_subject }}"
    CA_EXPIRE: "{{ ca_tools_ca_expire }}"
    SSL_SIZE: "{{ ca_tools_ssl_size }}"

- name: Generate server certificates
  ansible.builtin.command: generate-cert
  args:
    creates: "{{ host_vars_dir }}/{{ item.name }}/.ssl/server/cert.pem"
  environment:
    CA_CERT: "{{ ca_tools_ca_cert_dir }}/ca.pem"
    CA_KEY: "{{ ca_tools_ca_key_dir }}/ca-key.pem"
    SSL_KEY: "{{ host_vars_dir }}/{{ item.name }}/.ssl/server/key.pem"
    SSL_CSR: "{{ host_vars_dir }}/{{ item.name }}/.ssl/server/key.csr"
    SSL_CERT: "{{ host_vars_dir }}/{{ item.name }}/.ssl/server/cert.pem"
    SSL_CONFIG: "{{ host_vars_dir }}/{{ item.name }}/.ssl/server/openssl.cfg"
    SSL_SUBJECT: "{{ item.name }}"
    SSL_IP: "127.0.0.1,::1,{{ item.vars.ip | default('127.0.1.1') }}"
    SSL_DNS: "localhost,localhost.localdomain,{{ item.name }},{{ item.vars.fqdn | default(item.name) }}"
    SSL_SIZE: "{{ ca_tools_ssl_size }}"
    SSL_EXPIRE: "{{ ca_tools_ssl_expire }}"
  loop: "{{ hostvars | dict2items(key_name='name', value_name='vars') }}"

- name: Generate client certificates
  ansible.builtin.command: generate-cert
  args:
    creates: "{{ host_vars_dir }}/{{ item.name }}/.ssl/client/cert.pem"
  environment:
    CA_CERT: "{{ ca_tools_ca_cert_dir }}/ca.pem"
    CA_KEY: "{{ ca_tools_ca_key_dir }}/ca-key.pem"
    SSL_KEY: "{{ host_vars_dir }}/{{ item.name }}/.ssl/client/key.pem"
    SSL_CSR: "{{ host_vars_dir }}/{{ item.name }}/.ssl/client/key.csr"
    SSL_CERT: "{{ host_vars_dir }}/{{ item.name }}/.ssl/client/cert.pem"
    SSL_CONFIG: "{{ host_vars_dir }}/{{ item.name }}/.ssl/client/openssl.cfg"
    SSL_SUBJECT: "root@{{ item.name }}"
    SSL_SIZE: "{{ ca_tools_ssl_size }}"
    SSL_EXPIRE: "{{ ca_tools_ssl_expire }}"
  loop: "{{ hostvars | dict2items(key_name='name', value_name='vars') }}"
