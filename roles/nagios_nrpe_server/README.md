# nagios_nrpe_server

## Parameters

### `nagios_nrpe_server_state`

- default:

```
"enabled"

```

### `nagios_nrpe_server_local_cfg`

- default:

```
|
  allowed_hosts=monitoring.miskatonic.edu

```

