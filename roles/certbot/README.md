# certbot

## Parameters

### `certbot_state`

- default:

```
"enabled"

```

### `certbot_command`

- default:

```
|
  certbot --non-interactive --nginx --agree-tos --keep-until-expiring -d xnat.miskatonic.edu

```

