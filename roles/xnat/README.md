# xnat

## Parameters

### `xnat_state`

- default:

```
"enabled"

```

### `xnat_packages`

- default:

```
- "temurin-8-jdk"
- "tomcat9"

```

### `xnat_war`

- default:

```
"base_url": "https://api.bitbucket.org/2.0/repositories/xnatdev/xnat-web/downloads"
"name": "xnat-web"
"version": "1.8.6"

```

### `xnat_plugins`

- default:

```
- "base_url": "https://api.bitbucket.org/2.0/repositories/xnatdev/container-service/downloads"
  "name": "container-service"
  "version": "3.3.0-fat"
- "base_url": "https://api.bitbucket.org/2.0/repositories/xnatx/xnatx-batch-launch-plugin/downloads"
  "name": "batch-launch"
  "version": "0.6.0"
- "base_url": "https://api.bitbucket.org/2.0/repositories/icrimaginginformatics/ohif-viewer-xnat-plugin/downloads"
  "name": "ohif-viewer"
  "version": "3.4.0"

```

### `xnat_conf_properties`

- default:

```
|
  datasource.driver=org.postgresql.Driver
  datasource.url=jdbc:postgresql://localhost:6432/xnat
  datasource.username=xnat
  datasource.password=
  hibernate.dialect=org.hibernate.dialect.PostgreSQL9Dialect
  hibernate.hbm2ddl.auto=update
  hibernate.show_sql=false
  hibernate.cache.use_second_level_cache=true
  hibernate.cache.use_query_cache=true
  spring.http.multipart.max-file-size=1073741824
  spring.http.multipart.max-request-size=1073741824

```

### `xnat_prefs_ini`

- default:

```
|
  [siteConfig]
  siteUrl=https://xnat.miskatonic.edu
  adminEmail=harmitage@miskatonic.edu
  [notifications]
  smtpEnabled=false
  smtpHostname=fake.fake
  smtpPort=
  smtpUsername=
  smtpPassword=
  smtpAuth=

```

### `xnat_data_root`

- default:

```
"/data/xnat"

```

### `xnat_app_root`

- default:

```
"/opt/xnat-{{ xnat_war.version }}"

```

### `xnat_home`

- default:

```
"{{ xnat_data_root }}/home"

```

### `xnat_min_heap`

- default:

```
"256m"

```

### `xnat_max_heap`

- default:

```
"4g"

```

### `xnat_local_ca_cert`

- default:

```
"{{ inventory_dir }}/group_vars/all/.ssl/ca/ca.pem"

```

### `xnat_local_ssl_key`

- default:

```
"{{ inventory_dir }}/host_vars/{{ inventory_hostname }}/.ssl/client/key.pem"

```

### `xnat_local_ssl_cert`

- default:

```
"{{ inventory_dir }}/host_vars/{{ inventory_hostname }}/.ssl/client/cert.pem"

```

