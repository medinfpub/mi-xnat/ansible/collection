Ansible Collection - mi.xnat
============================

A collection of Ansible roles and playbooks for provisioning and
managing deployments of [XNAT](https://xnat.org) and related services.

Dependencies
------------

This collection target's Debian Linux and derivative distributions. It has been
developed and tested on following platforms:

- Debian:  11
- Python:  3.9
- Ansible: 2.13

Roles
-----

* [mi.xnat.ca_tools](roles/ca_tools/)
* [mi.xnat.certbot](roles/certbot/)
* [mi.xnat.common](roles/common/)
* [mi.xnat.controller](roles/controller/)
* [mi.xnat.dockerd](roles/dockerd/)
* [mi.xnat.nagios_nrpe_server](roles/nagios_nrpe_server/)
* [mi.xnat.nfs_client](roles/nfs_client/)
* [mi.xnat.nfs_server](roles/nfs_server/)
* [mi.xnat.nginx](roles/nginx/)
* [mi.xnat.xnat](roles/xnat/)
* [mi.xnat.xnat_openid_auth_plugin](roles/xnat_openid_auth_plugin/)

Each role has a `README.md` documenting the role's parameters

Playbooks
---------

### [mi.xnat.controller](playbooks/controller.yml)

Provision Ansible control node

### [mi.xnat.site](playbooks/site.yml)

Provision deployment consiting of an XNAT webserver and external Docker container service host.

License
-------

Apache-2.0
